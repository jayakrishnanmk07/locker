Gem::Specification.new do |s|
  s.name = %q{locker}
  s.version = "0.0.1"
  s.authors = ["Jayakrishnan"]
  s.date = %q{2019-09-01}
  s.summary = %q{A gem for encryption of sensitive data using aws kms envelope encryption with search functionality}
  s.files = [
    "lib/locker.rb",
  ]
  s.require_paths = ["lib"]

  s.add_dependency "attr_encrypted"
  s.add_dependency "aws-sdk-kms"
  s.add_dependency "activerecord"
  s.add_dependency "activesupport"
end
