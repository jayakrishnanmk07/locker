module Locker
  extend ActiveSupport::Concern

  class InvalidAttributeError < StandardError; end
  class AttributeMissingError < StandardError; end

  mattr_writer :number_of_stretches
  mattr_reader :locker_attributes
  mattr_accessor :aws_customer_master_key_alias_name
  mattr_accessor :aws_kms_client

  def self.config
    yield(self)
  end

  def self.number_of_stretches
    @@number_of_stretches || 100_000
  end

  included do
    before_validation :hash_locker_attributes, if: Proc.new { changes_to_locker_attributes.any? }

    @@locker_attributes ||= {}

    def self.locker(*locker_attributes)
      @@locker_attributes[self.name] = locker_attributes
      locker_attributes.each do |locker_attribute|

        #check for presence of: the hashing key, the encrypted encryption key column and the hashed value column
        if !encrypted_encryption_key_present?(locker_attribute, self)
          raise AttributeMissingError, "#{self.name} should have the column :encrypted_#{locker_attribute}_encryption_key"
        elsif !hashed_value_attribute_present?(locker_attribute, self)
          raise AttributeMissingError, "#{self.name} should have the column :#{locker_attribute}_hash"
        end

        # Set up attr_encrypted
        attr_encrypted locker_attribute, key: :"#{locker_attribute}_encryption_key"

        # Define custom finder methods
        [
          {
            finder_method_name: "find_by_#{locker_attribute}",
            ar_finder_method: :find_by,
            limiter_method: :take,
          },
          {
            finder_method_name: "find_all_by_#{locker_attribute}",
            ar_finder_method: :where,
            limiter_method: :all,
          },
        ].each do |options|
          define_singleton_method options[:finder_method_name] do |search_value|
            hashed_search_parameter = Locker.hashed_value(self, locker_attribute, send(:"#{locker_attribute}_hashing_key"), search_value)
            if column_attribute?("#{locker_attribute}_hash", self)
              send(options[:ar_finder_method], :"#{locker_attribute}_hash" => hashed_search_parameter)
            elsif store_attribute?("#{locker_attribute}_hash", self)
              store_key = stored_attributes.find { |key, store_attributes| store_attributes.map(&:to_s).include?("#{locker_attribute}_hash") }.first
              stored_attribute = "#{locker_attribute}_hash"
              where("#{store_key}->>'#{stored_attribute}' = ?", hashed_search_parameter).send(options[:limiter_method])
            end
          end
        end

        define_method "#{locker_attribute}_encryption_key" do
          if send("encrypted_#{locker_attribute}_encryption_key")
            aws_kms_client.decrypt(ciphertext_blob: [send("encrypted_#{locker_attribute}_encryption_key")].pack("H*")).plaintext
          else
            self.send("encrypted_#{locker_attribute}_encryption_key=", Locker.create_new_data_encryption_key)
            aws_kms_client.decrypt(ciphertext_blob: [send("encrypted_#{locker_attribute}_encryption_key")].pack("H*")).plaintext
          end
        end
      end
    end

    private

    def self.encrypted_encryption_key_present?(locker_attribute, klass)
      attribute_present?("encrypted_#{locker_attribute}_encryption_key", klass)
    end

    def self.hashed_value_attribute_present?(locker_attribute, klass)
      attribute_present?("#{locker_attribute}_hash", klass)
    end

    def self.attribute_present?(attribute_name, klass)
      column_attribute?(attribute_name, klass) || store_attribute?(attribute_name, klass)
    end

    def self.column_attribute?(attribute_name, klass)
      klass.column_names.include? attribute_name
    end

    def self.store_attribute?(attribute_name, klass)
      klass.stored_attributes.values.flatten.map(&:to_s).include? attribute_name
    end

    def hash_locker_attributes
      changes_to_locker_attributes.each do |attribute|
        if send(attribute).present?
          send("#{attribute}_hash=", Locker.hashed_value(self.class, attribute, self.class.send(:"#{attribute}_hashing_key"), send(attribute)))
        end
      end
    end

    def changes_to_locker_attributes
      changes.keys.map(&:to_sym) & @@locker_attributes.values.flatten
    end
  end

  def self.hashed_value(klass, locker_attribute, key, value)
    digest = OpenSSL::Digest::SHA512.new
    Base64.strict_encode64 OpenSSL::PKCS5.pbkdf2_hmac(value.to_s, key, number_of_stretches, digest.digest_length, digest)
  end

  def self.create_new_data_encryption_key(master_key_id = "alias/#{aws_customer_master_key_alias_name}")
    aws_kms_client.generate_data_key_without_plaintext(
      key_id: master_key_id,
      key_spec: "AES_256",
    ).ciphertext_blob.unpack("H*").first
  end
end
