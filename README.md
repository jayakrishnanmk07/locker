# Locker

Provides envelope encryption and secure search functionality for encrypted database columns in Rails applications

Built upon [attr-encrypted](https://github.com/attr-encrypted/attr_encrypted) and [aws-kms-sdk](https://docs.aws.amazon.com/sdk-for-ruby/v3/api/Aws/KMS.html)

Supports json columns as well


# Resources

*  [Envelope Encryption](https://docs.aws.amazon.com/kms/latest/developerguide/concepts.html#enveloping)
*  [Deterministic Encryption](https://en.wikipedia.org/wiki/Deterministic_encryption)
*  [Leakage](https://en.wikipedia.org/wiki/Deterministic_encryption#Leakage)

# Working

The encryption key for each sensitive information is encrypted using a master key stored in AWS KMS and the encrypted encryption key is stored in a database column

A keyed hash value([HMAC](https://en.wikipedia.org/wiki/HMAC)) of the sensitive informartion is computed and stored in another database column.

For searching, the hashed value is computed and then we do a database search on the hash column

# Usage

Prerequisite : Install and setup the gems [attr-encrypted](https://github.com/attr-encrypted/attr_encrypted) and [aws-kms-sdk](https://docs.aws.amazon.com/sdk-for-ruby/v3/api/Aws/KMS.html)

Add "locker" to your gemfile

```
gem "locker", git: "https://gitlab.com/jayakrishnanmk07/locker"
```

Install the gem

```
bundle install
```

Choose an alias name for your aws kms master key. For example, it can be `APPLICATION_NAME_MASTER_KEY` and  store it in your credentials or ENV

Add an initializer `config/initializers/locker.rb`

```
$aws_kms_client = Aws::KMS::Client.new(
  region: "your_aws_region",
  access_key_id: "your_aws_access_key_id",
  secret_access_key: "your_aws_secret_access_key"
)

Locker.config do |config|
  config.number_of_stretches = Rails.env.test? ? 1 : 100000
  config.aws_kms_client = $aws_kms_client
  config.aws_customer_master_key_alias_name = "your_master_key_alias_name"
end
```


Create a master key in AWS KMS and set an alias for the same : Go to your rails console and enter the following :

```
master_key_id = $aws_kms_client.create_key.key_metadata.key_id
$aws_kms_client.create_alias(alias_name: "alias/your_master_key_alias_name", target_key_id: master_key_id)
```

Consider the case of encryption of two attributes `ssn` and `account_number` for a `Person`

First create migrations to add the following String columns for storing the encrypted encryption keys and hashed values

`:ssn_hash`, `:encrypted_ssn_encryption_key` for ssn and `:account_number_hash`, `:encrypted_account_number_encryption_key` for accout_number

Now in the model, add the following code

```
class Person < ApplicationRecord

    include Locker
    
    locker :ssn, :account_number

    private

    def self.ssn_hashing_key
        "provide the hashing key from credentials"
    end

    def self.account_number_hashing_key
        "provide the hashing key from credentials"
    end
end
```

Note : You can get rid of the `attr_encrypted :attribute_name` setup from the model

Note: Storing of hashed value is done on `before_validation` callback.

So an `object.valid?` should be done before saving in case we need to do a `save(validate: false)` so that the hashed value is set correctly

Use `Person.find_by_ssn("value")` and `Person.find_all_by_ssn("value")` to perform searches on the encrypted column ssn

# To do

*  Add tests
*  Add generators for migrations
*  Add Key Rotation service
*  Add Support for hstore columns
